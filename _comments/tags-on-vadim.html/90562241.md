author : Rahul Jha
date: 17/06/2021 

 **SSL is Cool**

I am in no mood to write a blogpost explaining how I got rid of the dangerous warning pages that asked for confirmation everytime someone commented, warning them that their data isn't secure. (As if I collected anything of value, I am yet to come across a hacker who'll buy my _publically_ available Gitlab project ID). I was perplexed, because [TSTC](https://theshorttamperedclavier.github.io/) comments with the same configuration (apparently) wasn't throwing warnings every time I commented. I did a little digging to find out heroku provides a piggyback SSL certificate, and using *https* instead of HTTP will solve my problem. I know it's stupid, I should've used HTTPS from the start, but as they say, better late than never.