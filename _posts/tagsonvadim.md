title: Tags come to Vadim!!
url: tags-on-vadim.html
date: 2021-03-29
author: Rahul Jha
tags: code,vadim
summary : Adding tagging to Vadim static site generator

## Abstract:
So, today is Holi. And this is perhaps the only festival I do not like celebrating (JK, I do not like celebrating **any** festival). In order to keep myself busy while everyone else wastes water, I decided to add tagging to my blog, and by extension the Vadim static site generator. This blog is an account of the agony I had to go through.

## Tagging :
Static sites are cool, more so when [Github](http://www.github.io) offers to host them for free. I do not have to worry about convincing my dad to let me use his credit card to sign up for AWS free tier, or heroku in order to host my blog. But the problem with static sites is that it cannot run any client side code for, say, searching (this isn't a problem, this is the definition of a static site). So with searches out of the question, I decided to implement the next best thing - tagging. I had visited [Saksham Sharma's blog](https://www.sakshamsharma.com) some time back, and his *static* blog had tags. I believe most **established** SSGs have a feature for tagging built in, but I cannot copy them even if I wanted to because I do not know Ruby (the language Jekyll is written in) or Go (the language Hugo is written in), so I wrote a simple tagging program (one that is not even remotely pythonic, and would be the stuff of Guido van Rossum's nightmares) myself.
I had a simple mind map :
<ol>
<li> *Harvest* tags</li>
<li> Create a dictionary of tags </li>
<li> Use Jinja to create an html for each tag, for instance anecdote becomes 'tags anecdote.html' </li>
</ol>
I know I am not making myself very clear here, but that's the whole idea. 😸😸
I believed it would be easy, but coding is seldom easy. And like with all mind maps, this mind map too left a lot of dirty details out (for instance, did you know that one cannot have 2 brackets in the same block in Jinja?). <br> Long story short, I implemented it. Go check it out at [c17e](https://juggernautjha.gitlab.io/c17e). JK, you are already on that address.

## What Next
The current version of Vadim (vadim 0.0.2) is very crude, and I would definitely want to improve it. But there is only one Holi every year, and I guess I'll have to wait for another year to make further improvements to vadim.
