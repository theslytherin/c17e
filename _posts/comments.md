title: Comments
url: comments-on-a-staticsite.html
date: 2021-06-13
author: Rahul Jha
tags: computing, gitlab,
summary: When there's thrill there's a way. (Sorry Adan)

## Abstract
I hate Javascript. And the comments provider I used before used JS. I wanted to scrub my blog clean of that dirty javascript, but I still needed comments. So I wrote my own comments provider.

## Motivation
[The Short, Tampered Clavier](https://theshorttamperedclavier.github.io) is a blog run by four people who are smarter than me by many orders of magnitude. They use Jekyll, but that did not stop them from having comments. After doing some digging I found out they use staticman, an awesome way to add comments to static sites. I would have used Staticman, but it's written in Javascript. So, instead I stole the idea from them and wrote a terrible clone in Python. I call it staticboi.

The way it works is :
<ol>
<li> Someone hates their life (most important) </li>
<li> They visit this site and decide to leave a comment </li>
<li> staticboi hosted on heroku receives a request </li>
<li> It commits a markdown file to the gitlab repository </li>
<li> Gitlab pipeline builds the site </li>
</ol>

## Future
I'll want to include something that would allow users to keep track of the comments section. Ideally an rss feed for each page. It is going to be easy (or so I hope!!).
And yes, my encryption project is still on. As soon as I am done with that, I'll post all the inane epistles I've written to various people over the years online!!
