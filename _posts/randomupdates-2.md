title: Of Magnets and Monsoon
author: Rahul Jha
date: 2021-07-20
tags: jee, anecdote, delhi
url: magnets-monsoon.html
summary: The title is intended to make you ponder. You'd do well to think over it.

## Locked up in the Lockdown
The only reason I was taking this attempt at the JEE was because I had registered for it back in March, before Delhi was locked down. In the lockdown, I slacked off. I did not take breaks between bouts of studying, I studied between breaks. When NTA announced the exams I did what I do best, procrastination. I used to look at mocks, deem them too easy and start with another episode of Sarabhai v/s Sarabhai (that thing is worse than a drug, I tell you). It was not until the admit cards were released that I realized the need to put my act together. I was pretty good at Math and Physics, so they only required some brushing up. Chemistry was a different story altogether. I had set myself an ambitious target - 70 marks. And I spent the week between the release of the admit cards and the exam were mugging the NCERT up. On 19th, I received a message from a good friend, wishing me luck. That was one of the two good luck messages I was able to eke out of my contacts. Armed with a week worth of Chemistry and two good luck messages, I took the bull by the horns.

## JEE Take 3
The brief at NTA seems to be **annoy Rahul**. The kind folks at NTA seem to revel in my agony. I have been allotted centers in three different directions. This time, I was allotted at center in Ghazipur, at the east end of the city. It took us a little over an hour to get there, the folks at the center were helpful they even gave everyone a dapper face mask. And no, there was no senior citizen checking our admit cards. I had arrived in good time, so I had an hour to kill. I tried logging in but apparently the engineers at TCS had anticipated idiots like me and taken appropriate safeguards. Anyway, the exam began and the next three hours were a blur. I made a blunder in Chemistry, but I a relieved to say I still got the correct answer (thank god JEE does not go through the rough sheets). I took a total of 1 (one) leap of faith, and I am elated to say that I got the correct answer on that. The question involved magnets, so that's the first word of the title for you. Math was easy and I was done with the paper with an hour to spare. I went over the paper once again found out a couple of mistakes and corrected them. When I was finally **done** with the paper, I had 5 minutes to kill. I did the stupidest thing I could have done, I counted to 300. I had attempted 68 questions, and in the event that I get all of them correct, my score will be a cool 272. I do not intend to attempt the last session.

## Monsoon
I stepped out of the center to a downpour. My mother was waiting for me with an umbrella but god knows umbrella sellers give us a raw deal. The umbrella was the one I used in 7th grade and it was too small. I ended up getting drenched by the rain. I like rains, but only when I do not have to face them. It's kind of how I support Kashmiri separatist movements, because I do not have to face bomb blasts. Getting drenched irritates me, and getting drenched after a fabulous test irritates me to no end. The rain killed my enthusiasm. The 90 minute journey back home was excruciating. The only saving grace was a plate of Samose (potato pastry lol). I wish to better my percentile this time but I doubt if that would make me happy.

Signing off,
Rahul Jha
Maybe-an-IITIAN
