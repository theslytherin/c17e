title: Objectively Determining Coolness
url: elo-1.html
date: 2022-1-31
author: Rahul Jha
tags: bad math, economics, go, ors
summary: And other hot takes

## Prologue
This venture started the way most of my ventures start - my crush #728 went off with someone else. Now, when confronted with a situation like this, any sane person will do whatever sane people usually do. They'll probably take up a new hobby, study to improve their MTH marks, and maybe try their luck elsewhere. On the other hand, I have never been bothered by societal notions of sanity. So I set about devising a plan to zero in on the guy (yes, I have no idea who she ran away with). In an online semester, this is easier said than done. And I had just one piece of information - I know a guy she rejected. So doing what economists and wannabe economists and generic ass-holes do, I assumed humans are entirely rational creatures. (I am writing for an inane blog instead of studying, contradicting my assumption of rationality. I realized this after I was halfway through the project, so too late. Oops.)
A rational creature will choose A over B iff A is cooler than B in most ways. Coolness is an abstract idea because a freshman maintaining a blog is supposed to be cool. Still, my DMs say otherwise. Maybe coolness is some combination of intelligence (marks are a good proxy), looks (my selfies bound the set from below), and a factor that explains how fun someone is. I know I am not being obvious, and I would attribute that to the fact that as of writing, the midsem break has ended. In principle, if we can assign a numerical value to each of these attributes, we can perform a regression to determine the coefficients. In practice, though, this requires a LOT of data, and even by my conservative estimates, I'll be a guest of the DCP(Prison) for seven years if I attempt something like this.

## The Plan
It is plausible that we have a rough formula of sorts for determining coolness hard-coded in our brains. Because we all seem to agree that [Xilleilahi](https://twitter.com/Xilleilahi) is cool while Mayawati is not. In principle, we can figure out that formula by circulating a Google form that asks for the coefficients and then averaging all responses. Another Google form can ask everyone to rate everyone on the three parameters. Then, one can create a leaderboard by calculating the CQ (coolness coefficient, hehe) using the new-found formula. It is impractical for two reasons: the sheer number of students in my year. The more personal reason is that I despise Javascript (and am afraid of ending up at the bottom of the leaderboard). A much simpler method is to ape Zuckerberg and to ask everyone to rate two people at a time. In principle, the actual rankings should come very close to the rankings compiled by taking two people at a time. Of course, it seems incredulous. And that's the purpose of this post, to verify that such a ranking system would work. (This is a stupid blog ELO is used to rank chess players, so it is obviously perfect).

## The Experiment
I used the formula **_coolness = 0.45*looks + 0.2*intelligence + 0.35*fun_** because why not?. My OG ranking program used this to rank people. I then generated a database of 120 people and assigned values to the three attributes randomly. Every person started with an ELO rating of 1000. Each attribute can have a value in the range [0,1], and (1,1,1) equals Mawra Hocane. A server returned two people on each query. I wrote a client that queried the server for people and rated them using a similar formula (coefficients varied to account for non-uniformities). I also set the percentage of idiots (people who vote for CPM even when it is losing) to 10% (a friend suggested that I could've filled every idiot's place, but let's ignore her). I then ran the server and obtained the final ELO ratings after a long, long time (4.5 minutes per 10000 iterations, i5 sucks). Then I ranked the population according to their ELO rating. And finally, I scattered (og rank, elo rank). The results were, well, frustrating at first and perfect finally.
I have compiled all screenshots in a PDF file (primarily because my static site generator does not elegantly render jpegs. Sad, I know.) [here](https://juggernautjha.gitlab.io/assets/research/pictures.pdf).

## Conclusion
In no particular order:
<ul> <li>TLDR, ELO works splendidly.</li>
<li> The regression parameters do not change appreciably after 20000 (every person ranked 33 times) iterations. So waiting for 15 minutes to get an improvement of 0.01% is a bad idea. </li>
<li> Cloning facemash in IITK will be very difficult </li>
<li> I am cool </li>
<li> Even if I determine who the bastard is, I don't know what I'll do with that information</li>
</ul>

## Epilogue
I botched up my chances with crush #729. So no matter how cool you are, you should probably work on them DMing skillz. Also, my namesake in the Department of Earth Sciences is contesting for the Senate, so if he wins I may end up becoming the counterfeit Rahul Jha in IITK. Life is hard. You can find all the code I used for this blog [here](https://gitlab.com/juggernautjha/elo-test).

## Screengrabs and Addenda
[Server Serving the Client](https://juggernautjha.gitlab.io/assets/research/screengrab.png) <br><br>
**_5% Deviation_** <br>
[Plot](https://juggernautjha.gitlab.io/assets/research/0.02_20000.svg) [Best Fit Line](https://juggernautjha.gitlab.io/assets/research/0.02_20000_regression.png) [CSV Dataset](https://juggernautjha.gitlab.io/assets/research/0.02_20000.csv) <br><br>
**_25% Deviation_** <br>
[Plot](https://juggernautjha.gitlab.io/assets/research/0.1_20000.svg) [Best Fit Line](https://juggernautjha.gitlab.io/assets/research/0.1_20000_regression.png) [CSV Dataset](https://juggernautjha.gitlab.io/assets/research/0.1_20000.csv) <br><br>
**_50% Deviation_** <br>
[Plot](https://juggernautjha.gitlab.io/assets/research/0.2_30000.svg) [Best Fit Line](https://juggernautjha.gitlab.io/assets/research/0.2_30000_regression.png) [CSV Dataset](https://juggernautjha.gitlab.io/assets/research/0.2_30000.csv) <br><br>
**_50% Deviation, but a dumb Rahul_** <br>
[Plot](https://juggernautjha.gitlab.io/assets/research/0.2_50000.svg) [Best Fit Line](https://juggernautjha.gitlab.io/assets/research/0.2_50000_regression.png) [CSV Dataset](https://juggernautjha.gitlab.io/assets/research/0.2_50000.csv)
