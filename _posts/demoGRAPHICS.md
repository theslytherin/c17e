title: demoGRAPHICS & Other Sus Ideas
url: demoGRAPHICS.html
date: 2022-10-05
author: Rahul Jha
tags: data, scraping
summary: Y21s deserve a survey too!

## Abstract
Last year, IITKGP decided to declare the JEE Advance result on Vijayadashmi. My Chemistry marks ensured that I was too depressed to blog. A lot of things have changed in the intervening 12 months, I no longer have Chemistry PTSD (jklol, I do) and I have something interesting to blog about.

## The Freshers Survey
Every year, 1200 clueless idiots enter the portals of IIT Kanpur. And Vox Populi, the campus journalism body conducts an informal survey. The survey has fun questions like expected CPI (the mean of the answers is usually well above my projected CPI if I score a 10 in every semester), expected package (legend has it that the sum of expected packages is a tad higher than Burundi's domestic product) and not-so-fun questions like the expected number of girl/boyfriends. I do not look at that metric because I am not a masochist. Our batch was not surveyed. I am guessing it was because of CoVID and not because my friends (and therefore my sample of the population) are about as interesting as a one sided test match where Pakistan has the upper hand. The survey also included some handy stats like the number of students every state sends to IITK. Stuff like this comes in handy when you are defending the claim that Delhi is the greatest city on earth. One would think that people in IITs are smart enough to accept that this claim as an axiom but reality, like my existence, is often dissapointing. Our batch does not have such stats. It is sad, I know. 

## Enter PClub
The Programming Club, known colloquially as PClub (and not PeeClub which may or may not be a category on certain illicit websites) maintains a searchable database of all students, PhDs and MTechs. It can only be accessed on the intranet (thank god) and has important information like blood group, city, room numbers - even though that information is terribly innacurate. A senior I kinda liked was a Hall 3 hoodlum according to [the website](https://search.pclub.in). He convinced me it was clerical error -, and ofcourse, departments (again inaccurate). Notice any similarity between data Vox asks for and the data already on Students Search? Yup, we can _conduct_ a little survey of sorts for our batch. By we I mean me and my laptop, because my friends are the human equivalent of classic salted Lays, trustworthy but bland. The next few sections contain some results and some commentary. The Jupyter Notebook is linked [here](https://juggernautjha.gitlab.io/assets/blog_images/research.ipynb) [csv file](https://juggernautjha.gitlab.io/assets/blog_images/y21.csv) if someone (some man of culture) wants to perform some _dAtA ScIEnCe_ on the dataframe.

## Hall Allocation
There are a few things I am prepared to pick a fight for. The primacy of Linux over every other operating system, the fact that Delhi Metro is the best thing to happen to India since my birth (the two events were 5 days apart), and that Hall 12 is by considerable margin the sexiest hall on campus. While the first two statements are just facts and I do not foresee any opposition, the third statement - while completely true - is a tad controversial. People from every hall (yup, unfortunately even hall 2) believe that their hall is the best. They give totally-not-logical arguments that their hall won Galaxy when Indira Gandhi was still alive. Listen to Avik Pal and come back to the present bitches. So anyway, people have a weird connection to a _randomly_ alloted hall. Yup, hall allocation is indeed random. Unlike some places which must not be named because I intend to keep this page fit for general consumption, IITK attaches fuck all weight to your JEE Rank - or the lack thereof. Everyone is randomly assigned to a hall, keeping in mind the strength (duh). This should result in clusters forming. And indeed they do. Note that hall 12 and hall 5 have the highest number of people in each branch, simply because they the largest halls. Also note the obscenely high number of SDS folks in Hall 5. Ofcourse, <em>obscenely high</em> is relative. I did not include hall 6 in the graph because I was too lazy to copy those numbers off of JoSAA's seat matrix.
<br>
<img src="https://juggernautjha.gitlab.io/assets/blog_images/branch.jpg" >
<br>

## Blood Group
I do not know why I wasted time doing this. Blood Groups are random (atleast they do not have a measurable impact on performance in the JEE), hall allocation is random (unless you have a redundant X chromosome). Therefore the distribution of blood groups across halls is not supposed to be insightful. Good luck if you make anything out of this tho. My only comment is that Hall 5 for some reason has the highest number of AB+ people whereas hall 12 has the highest number of altruistic O+ chads. AB+ is also the blood-group equivalent of Pakistan - always on the lookout for aid. 
<br>
<img src="https://juggernautjha.gitlab.io/assets/blog_images/blood.jpg" >
<br>

## Home State
I did not look at hall allocation by state. Chiefly because I am scared of my friends brutalizing me if I make one more state joke and claim that Hall 12 (whose reidents are literally called **Marathas**) is the coolest because it has the highest number of my Bihari brethren. It is a vanilla graph, but biy would you look at the sexy shape. It is almost periodic with decreasing amplitude. Also a note on the order of states, Gujarat is the first state not because I am a ModiJee fanboi or I have Gujju friends. It is the first state in the list because our resident chad Atulya Sundaram happens to have roll number 1 and he is from Gujjuland. 
<br>
<img src="https://juggernautjha.gitlab.io/assets/blog_images/state.jpg" >
<br>

## Sus Ideas
First, let me define sus. Any action in contravention to the ever changing Rahul Jha's [code of conduct](https://juggernautjha.gitlab.io/assets/coc.txt). Not having a girlfriend is right up there with not leaking state secrets and not cheating on a Math test. Therefore, having a girlfriend makes one sus. I promise this is not a cope. Trust me. <strike>I will totally not amend the CoC if I get a girlfriend</strike>. Anyway, here is a question. Who do you think gets a better deal when guys propose and girls reject? The obvious answer is the guy's roommate. But other than that, it is the bois. I was/am studying matching theory for a project and there is a result that says that a matching which results from the above arrangement is inefficient. By inefficient I mean that there is atleast one matching where every girl either gets the same boy and atleast one girl gets a boy she preferred more. How to reach the most efficient matching from the girls' perspective, you ask? Simple, let girls propose and guys reject :).

## Epilogue
I need to work on this blog. Aaaargghhhh.