title: Boredom!!
url: bored-to-death.html
date: 2021-02-22
author: Rahul Jha
tags: timepass,life,maza
summary: Apologies of a mad man (sorry Khushwant Singh).

## Abstract (🤷‍♀️)
In about 36 hours, I will be writing (not exactly, but you get the idea, right?) the first iteration of JEE Main, and while I am reasonably confident about my preparation (and have a prejudice that JEE Main is an easy exam), it is incredibly difficult to not to start another mock test. Someone I've recently become very frightened of suggested that I write something, so here goes nothing 😁 (I recently modified the rendering code to parse emojis, so this blog might contain a shitload of superfluous emoticons).
I intend to post this on [c17e](https://www.c17e.github.io), the semi-regular blog I moderate, and therefore, I don't think the working of a pnp transistor (in common-emitter mode no less) will be an appealing article, and for similar reasons, my audience (1 and a half people at last count) will resonate with the geopolitics of Kazakhstan, so like all other clueless writers, I will pick up a very abstract topic that neither I nor you have the remotest idea about. And the topic I've chosen is (🥁🥁🥁) **self-deprecation**.

## Self Deprecation
I am, in no particular order :
<ul>
<li> <strong>A grammar nazi: </strong></li>
Even I don't know when I became a grammar nazi (or as my conscience puts it, became a rebel without a cause). I guess it has something to do with the fact that I lack a lot of qualities boys idolize: I look ugly (more on that later), have a BMI of 14, and suck at sports. So, to make myself stand out - to carve a niche for me, I'd say, I started correcting other people's grammar. 
I believe it gives me legitimacy (it doesn't) and after this xkcd, I'll try to curb my habit but I can't promise you anything.</br>
<img src="https://imgs.xkcd.com/comics/fashion_police_and_grammar_police.png" alt="grammar" style="height:500px;"/>
</br></br>
<li> <strong>Inexplicably Ugly: </strong></li>
I can say I look like a scarecrow, but that'd be an insult to the scarecrow. So, needless to say, I've not found much favor with the X chromosome. The ugly mug of mine does not prevent me from being a narcissist 😊😊. In case you have not had enough flying shit today, I present to you my picture.<br>
<img src="https://juggernautjha.gitlab.io/assets/profile.jpg" alt='kk' style="width:120px"> 
<br><br>
<li><strong> Prejudiced: </strong></li>
I am <b>not</b> as liberal as my friends would probably expect someone (you know, for someone who pretends to know everything) like me to be. I do not share food and water, that is very wrong. I do not approach people and have an air of superiority about me. <br>
<img src="https://imgs.xkcd.com/comics/atheists.png">
</ul>
<br>
So, I guess it is understandable why I have next to no friends - why would someone be friends with a narcissist bastard who will correct their grammar every other day and will add nothing to the aura around them? I do feel left out, and alone at times but then I look at myself, and I get all the answers. I'd say I'm not going to change myself for something as transitory as friendship, but the fact is I am way past the point of no return, and there is absolutely no way I can change myself.

