title: Stupid Startup Ideas - 2
author: Rahul Jha
date: 2021-10-12
tags: ssi, startup
url: startup-2.html
summary: Pegasus is sus. It's Udta Ghoda time

You are eating a piping hot dosa when a phrase comes into your mind, 'Bailar Conmigo'. You know you have read this phrase in the Instagram bio of a follower of your follower. You are taken by the sudden urge to find out whose bio it was. You fish out your laptop (because naturally, you do not own a phone), ask the waiter for the WiFi password, and log in to Instagram. By now, you have all but forogotten why you logged in, but the free WiFi is welcome. You go start resume your torrents. Life is good. That night, when you are about to sleep, the phrase hits you again. You log in, find out you have 27 followers, but all your followers have more than 500 followers. You do not fancy checking 13500 bios. You smother your curiosity, and sleep. Wouldn't it be amazing if someone maintained a tool that would scrape the bios of all your followers, and their followers and so on. An Instagram bio cannot be hidden, and web scraping is a child's play (once you get through those pesky login pages). I wonder why no one has attempted this. Wouldn't it be great if you and I did it together? I also happen to know a very good VC, the Government of India. After the Pegasus fracas, I hear they are steering clear of imported spying software. They will only be too willing to buy a made-in-India Pegasus clone. I also have a name in mind, **Udta Ghoda**.

You know the drill, send me your CV (or class 12th marksheet, whichever is more impressive) at kashmir_maange_aazadi@topsecret-69.com .