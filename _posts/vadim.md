title: Vadim
url: vadim-blyat.html
date: 2021-01-23
author: Rahul Jha
tags: computing,vadim,code
summary: Making of Vadim static site generator, Sheena argument parser and a few rants.



## The Need
Last summer, I took part in Google code-in (for the black t-shirt, of course) and one of the tasks involved signing my Github repository by using a GPG key. I won't bore you with the details, but I've forgotten my passphrase. Now, I decided to start this blog a few weeks ago but to my utter despair, I could not use any of the famous static site generators like jekyll or hugo. So I decided to tweak an static site generator (a pretty basic one) from <https://rahmonov.me/posts/static-site-generator/> and it works pretty satisfactorily for me. It does not have all the bells and whistles of, say, Jekyll but it does what it says.

1. I do not have to edit the HTML every time I discover a typo in one of my blogs.
2. I do not have to worry about committing a syntax error in one of the blog posts, because all of them are generated from a common template.

In short, it does everything a fairly simple static site generator should do.

## Sheena Argument Parser

I am a great fan of docopt, having used it in one of my previous projects, but I find is very _un-nerdy_ to type the usage string. So I created Sheena argument parser, a 30 line python script that takes a JSON file and maps it to arguments. Now, Vadim does not need an argument parser because it has exactly two options - initialize and compile, but I thought it would be cool for me to code one. I will be posting the source code on my primary Github.

**Note** : Sheena is an obfuscation of someones name in ROT13. If you are curious about the real name, you can always de-obfuscate it.
<div>

</div>

## Future of Vadim (and Sheena)

To be frank, there isn't. I am a lazier than the laziest person you've ever met. I created Vadim because I wanted a static site generator for my blog and now that I have one that works satisfactorily for me, I do not see the need to spend anymore time on this. I'll be updating it as and when I find flaws that are big enough to get me started. If you are looking for a battle tested static site generator, you are at the wrong place as on 23-01-2021. Sheena, on the other hand will definitely be improved. I'll test is for larger and larger applications and I guess Sheena will be me pet project for the near future.


## Update (14 Feb 2020)
I feel very stupid admitting this, but I finally remember my GPG passphrase and can commit stuff from the command line, that does not change much, but I can use GIT to manage large projects (which are rarer than the rarest comics now a days).
