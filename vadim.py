import time, hashlib, pickle, webbrowser, os
import json

#! Today we refactor, mashallah.
def init(site_name):
    directory = ['_comments', "_posts", "public/posts",  "public/css", "template", "public/tags", "public/posts"]
    cd = os.getcwd()
    for i in directory:
        path = os.path.join(cd,i)
        os.mkdir(path)
        os.chdir(path)
        ftc = site_name + directory[i]
        print(ftc)
        os.chdir(cd)
    with open('config.json', 'w') as configfile:
        pass

with open("config.json") as config:
    config_dict = json.load(config)
    if config_dict['pagination']['state'] == 'True':
        PAGINATE = True
        BLOGS_PER_PAGE = eval(config_dict['pagination']['per page'])


def paginate(index_posts_metadata, bpp):
    from jinja2 import Environment, PackageLoader
    env = Environment(loader=PackageLoader('vadim','template'))
    index_template = env.get_template('indexpg.html')
    pages = (len(index_posts_metadata)//bpp) + 1
    for i in range(pages):
        try:
            temp_posts_metadata = index_posts_metadata[:bpp]
        except:
            temp_posts_metadata = index_posts_metadata
        index_posts_metadata = index_posts_metadata[bpp:]
        if i == 0:
            index_html_content = index_template.render(posts = temp_posts_metadata, prev = '', number = i+1, next=i+2)
            with open('public/index.html', 'w', encoding="utf-8") as file:
                file.write(index_html_content)
        elif i == pages-1:
            if i != 1:
                index_html_content = index_template.render(posts = temp_posts_metadata, prev = i, number = i+1, next=i+1)
                with open('public/index{0}.html'.format(i+1), 'w', encoding="utf-8") as file:
                    file.write(index_html_content)
            elif i == 1:
                index_html_content = index_template.render(posts = temp_posts_metadata, prev = '', number = i+1, next=i+1)
                with open('public/index{0}.html'.format(i+1), 'w', encoding="utf-8") as file:
                    file.write(index_html_content)

        elif i == 1:
            index_html_content = index_template.render(posts = temp_posts_metadata, prev = '', number = i+1, next=i+2)
            with open('public/index{0}.html'.format(i+1), 'w', encoding="utf-8") as file:
                file.write(index_html_content)

        else:
            index_html_content = index_template.render(posts = temp_posts_metadata, prev = i, number = i+1, next=i+2)
            with open('public/index{0}.html'.format(i+1), 'w', encoding="utf-8") as file:
                file.write(index_html_content)
        print("index generated")

def compile():
    import os
    from datetime import datetime
    from jinja2 import Environment, PackageLoader
    from markdown2 import markdown
    POSTS = {}
    try:

        for mdpost in os.listdir('_posts'):
            file_path = os.path.join('_posts', mdpost)
            with open(file_path, 'r', encoding = 'utf-8') as file:
#                print(file_path)
                POSTS[mdpost] = markdown(file.read(), extras = ['metadata'])
    except:
        pass

#give tags only if the user wants
    for i in POSTS:
        POSTS[i].metadata['tags'] = POSTS[i].metadata['tags'].split(',')
        POSTS[i].metadata['tags'] = [i.strip() for i in POSTS[i].metadata['tags']]
    for i in POSTS:
        tgtmp = []
        for j in POSTS[i].metadata['tags']:
            lsn = dict()
            lsn['id'] = j
            lsn['url'] = '{0}.html'.format(j)
            tgtmp.append(lsn)
        POSTS[i].metadata['tags'] = tgtmp

    POSTS = {
    post: POSTS[post] for post in sorted(POSTS, key=lambda post: datetime.strptime(POSTS[post].metadata['date'], '%Y-%m-%d'), reverse=True)
    }
    print(POSTS)
    index_posts_metadata = [POSTS[post].metadata for post in POSTS if 'noindex' not in POSTS[post].metadata]
    print(index_posts_metadata)
    for i in index_posts_metadata:
        if 'noindex' in i:
            print(index_posts_metadata[index_posts_metadata.index(i)])
            del index_posts_metadata[index_posts_metadata.index(i)]
    #print(index_posts_metadata)
    if PAGINATE == True:
        paginate(index_posts_metadata, BLOGS_PER_PAGE)
    else:
        env = Environment(loader=PackageLoader('vadim','template'))
        tag_pager(POSTS, env)
        index_template = env.get_template('index.html')
        index_html_content = index_template.render(posts=index_posts_metadata)
        with open('public/index.html', 'w', encoding="utf-8") as file:
            file.write(index_html_content)

    xml_final = []
    for i in POSTS:
        xml_metadata = dict()
        xml_metadata['content'] = POSTS[i]
        for j in POSTS[i].metadata:
            xml_metadata[j] = POSTS[i].metadata[j]
        xml_final.append(xml_metadata)
    misc_data = {
        'builddate' : time.ctime()
    }
    try:
        env = Environment(loader=PackageLoader('vadim','template'))
        tag_pager(POSTS, env)
        #index_template = env.get_template('index.html')
        post_template = env.get_template('layout.html')
        toml_template = env.get_template('toml.html')
        #index_html_content = index_template.render(posts=index_posts_metadata)
        toml_content = toml_template.render(posts = xml_final, meta = misc_data)
        with open('public/rss.toml', 'w', encoding="utf-8") as file:
            file.write(toml_content)
        xml_template = env.get_template('rss.html')
        xml_content = xml_template.render(posts = xml_final, meta = misc_data)
        with open('public/rss.xml', 'w', encoding="utf-8") as file:
            file.write(xml_content)
        #with open('public/index.html', 'w', encoding="utf-8") as file:
        #    file.write(index_html_content)
        for post in POSTS:
            if 2 == 2:
                post_metadata = POSTS[post].metadata
                post_data = {
                'content': POSTS[post],
                'title': post_metadata['title'],
                'date': post_metadata['date'],
                'author' : post_metadata['author'],
                'tags' : post_metadata['tags'],
                'url' : post_metadata['url']
                }
                try:
                    comments = getcomments(post_metadata['url'])
                except:
                    comments = []

                #print(comments)

                try:
                    masterlen = len(comments)
                except:
                    masterlen = 0

                if masterlen == 1:
                    post_html_content = post_template.render(post=post_data, comments = comments, number = masterlen, what = "Comment")
                    #print(post_html_content)
                elif masterlen == 0:
                    post_html_content = post_template.render(post=post_data, comments = [], number = masterlen, what= "Comments")
                else:
                    post_html_content = post_template.render(post=post_data, comments = comments, number = masterlen, what= "Comments")
                    #print(post_html_content)


                    #stupiditu
                post_file_path = 'public/posts/{slug}'.format(slug=post_metadata['url'])
                with open(post_file_path, 'w+', encoding="utf-8") as file:
                    file.write(post_html_content)
        return "Success"
    except Exception as e:
        print(e)


def getcomments(url):
    import os
    from markdown2 import markdown
    blog_comment_metadata = []
    COMMENTS = dict()
    try:
        for comment in os.listdir('_comments/{0}'.format(url)):
            file_path = os.path.join('_comments/{0}'.format(url), comment)
            with open(file_path, 'r', encoding = 'utf-8') as file:
                COMMENTS[comment] = markdown(file.read(), extras = ['metadata'])
            meta = COMMENTS[comment].metadata
            meta_dic = {
                'author' : meta['author'],
                'date' : meta['date'],
                'comment': COMMENTS[comment]
            }
            blog_comment_metadata.append(meta_dic)
 #       print(blog_comment_metadata)
        return blog_comment_metadata
        #FileNotFoundError
    except:
        return blog_comment_metadata

    #print(blog_comment_metadata

#getcomments('movingtogitlab.html')

def tag_pager(DICT, env):
    tags = []
    for i in DICT:
        for j in DICT[i].metadata['tags']:
            tmpdict = dict()
            tmpdict['post_url'] = DICT[i].metadata['url']
            tmpdict['post_id'] = DICT[i].metadata['title']
            tmpdict['date'] = DICT[i].metadata['date']
            tmpdict['author'] = DICT[i].metadata['author']
            tmpdict['summary'] = DICT[i].metadata['summary']
            tmpdict['id'] = j['id']
            tmpdict['url'] = j['url']
            tags.append(tmpdict)
    distinct_tags = []
    for i in tags:
        if i['id'] in distinct_tags:
            continue
        else:
            distinct_tags.append(i['id'])
    tag_template = env.get_template('tag.html')
    for i in distinct_tags:
        tempdict =[] #misnomer
        for j in tags:
            if j['id'] == i:
                tempdict.append(j)
        tag_html = tag_template.render(posts=tempdict, tag = i)
        tag_file = 'public/tags/{slug}'.format(slug=tempdict[0]['url'])
        with open(tag_file, 'w+', encoding="utf-8") as file:
                      file.write(tag_html)
